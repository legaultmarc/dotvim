" The color scheme
" colorscheme wombat
colorscheme github

" The color of the insert mode cursor
highlight iCursor guibg=orange
set guicursor+=i:ver25-iCursor

" Remove the menu bar
set guioptions-=m

" Remove toolbar
set guioptions-=T

" Remove the visual bell
set novb

" Reset the sign column colors
highlight clear SignColumn" Find the errors

" Show the line at col 80
execute "set colorcolumn=" . join(range(81,335), ',')
highlight ColorColumn ctermbg=darkgrey guibg=#DCDCDC
" highlight ColorColumn ctermbg=darkgrey guibg=#333333
