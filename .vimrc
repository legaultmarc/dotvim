execute pathogen#infect()

syntax enable
set background=dark
colorscheme wombat

" Indentation and tabbing
set smartindent
set tabstop=4
set shiftwidth=4
set expandtab
set number

" Automatic line break at 80 chars bitches
set textwidth=80
set linebreak
" Show the line at col 80
execute "set colorcolumn=" . join(range(81,335), ',')
highlight ColorColumn ctermbg=darkgrey

" Guides (\ig)
set ts=4 sw=4 et
let g:indent_guides_start_level = 2
let g:indent_guides_guide_size = 1
" let g:indent_guides_enable_on_vim_startup = 1

let g:indent_guides_auto_colors = 0
autocmd VimEnter,Colorscheme * :hi IndentGuidesOdd  guibg=#e3e3e3 ctermbg=3
autocmd VimEnter,Colorscheme * :hi IndentGuidesEven guibg=#dbdbdb ctermbg=4

